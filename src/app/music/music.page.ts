import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as howler from 'howler';

@Component({
  selector: 'app-music',
  templateUrl: './music.page.html',
  styleUrls: ['./music.page.scss'],
})
export class MusicPage implements OnInit {

  // Member variables
  music: any;
  image: string;
  audio: string;

  constructor(public router: Router, public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.audio = this.activatedRoute.snapshot.paramMap.get('audio');
    this.music = new howler.Howl({ src: ['../../assets/audio/' + this.audio + '.wav']});
    this.image = '../../assets/imgs/' + this.audio + '.jpg';
  }

  ionViewWillEnter() {
    console.log('playing audio');
    this.music.play();
    this.music.loop(true);
  }

  ionViewDidLeave() {
    console.log('pausing audio');
    this.music.pause();
  }

  goToCreditsPage() {
    this.router.navigateByUrl('credits');
  }

}
